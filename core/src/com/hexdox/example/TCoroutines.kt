package com.hexdox.example

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import kotlinx.coroutines.*

class TCoroutines : ApplicationAdapter() {
    private val TAG = "robo-c"

    override fun create() {
        start()
    }

    private fun start() {
        Gdx.app.log(TAG, "start test")

        GlobalScope.launch {
            suspendFunction()
            delay(1000)
            Gdx.app.log(TAG, "World!")
        }

        runBlocking {
            delay(1000)
            Gdx.app.log(TAG, "Hello parallel world")
        }

    }


    private suspend fun suspendFunction() = coroutineScope {
        launch {
            delay(200)
            Gdx.app.log(TAG, "[Ehh..]")
        }
        Gdx.app.log(TAG, "Hello")
    }
}
